import typescript from 'rollup-plugin-typescript2'
import commonjs from 'rollup-plugin-commonjs'
import external from 'rollup-plugin-peer-deps-external'
import postcss from 'rollup-plugin-postcss-modules'
import postCssCopy from 'postcss-copy'
// import postcss from 'rollup-plugin-postcss'
import resolve from 'rollup-plugin-node-resolve'
import url from 'rollup-plugin-url'
import svgr from '@svgr/rollup'
import pkg from './package.json'
import fs from 'fs'
import glob from 'glob'
import autoprefixer from 'autoprefixer'

/* initialize CSS files because of a catch-22 situation:
   https://github.com/rollup/rollup/issues/1404 */
glob.sync('src/**/*.css').forEach((css) => {  // Use forEach because https://github.com/rollup/rollup/issues/1873
  const definition = `${css}.d.ts`
  if (!fs.existsSync(definition))
    fs.writeFileSync(definition, 'const mod: { [cls: string]: string }\nexport default mod\n')
})

export default {
  input: 'src/index.tsx',
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      exports: 'named',
      sourcemap: true
    },
    {
      file: pkg.module,
      format: 'es',
      exports: 'named',
      sourcemap: true
    },
    {
      file: pkg.umd,
      name: "pewocomcom",
      format: 'umd',
      exports: 'named',
      sourcemap: true
    }
  ],
  plugins: [
    external(),
    postcss({
      // modules: true,
      extract: 'dist/index.css',
      plugins: [
        autoprefixer(),
        postCssCopy({dest: 'dist', template(fileMeta) {
          return "./assets/" + fileMeta.hash + "." + fileMeta.ext + fileMeta.query;
        }})
      ],
      writeDefinitions: true,
      // namedExports: name => "pewoComCom__" + name
    }),
    url(),
    svgr(),
    resolve(),
    typescript({
      rollupCommonJSResolveHack: true,
      clean: true
    }),
    commonjs()
  ]
}
