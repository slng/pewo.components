/// <reference types="react" />
export * from "./types";
export declare const getProperties: <T extends import("./types").ComponentsEnum>(component: T, props: import("./types/componentDataTypes").ComponentMap[T]) => import("./types").ComponentsProps<T>;
export declare const getPropTemplate: <T extends import("./types").ComponentsEnum>(components: T) => import("./types/componentFunctionTypes").FuncMap[T];
export declare const Component: (props: import("./types").ComponentsProps<import("./types").ComponentsEnum>) => JSX.Element;
export declare const Headline: (props: import("./types/componentDataTypes").Headline1Data) => JSX.Element;
export declare const H2WithText: (props: import("./types/componentDataTypes").HeadlineWithTeaserData) => JSX.Element;
export declare const H3WithText: (props: import("./types/componentDataTypes").HeadlineWithTeaserData) => JSX.Element;
export declare const HeadlineEyebrownText: (props: import("./types/componentDataTypes").HeadlineEyebrownTextData) => JSX.Element;
export declare const HeadlineWithTeaser: (props: import("./types/componentDataTypes").HeadlineWithTeaserData) => JSX.Element;
export declare const HeadlineWithTeaser2: (props: import("./types/componentDataTypes").HeadlineWithTeaserData) => JSX.Element;
export declare const Picture: (props: import("./types/componentDataTypes").PictureData) => JSX.Element;
export declare const Picture2: (props: import("./types/componentDataTypes").Picture2Data) => JSX.Element;
export declare const Picture3: (props: import("./types/componentDataTypes").Picture3Data) => JSX.Element;
export declare const PictureHeadlineText3: (props: import("./types/componentDataTypes").PictureHeadlineText3Data) => JSX.Element;
export declare const PictureWithText: (props: import("./types/componentDataTypes").PictureWithTextData) => JSX.Element;
export declare const TextBlock: (props: import("./types/componentDataTypes").TextBlock1Data) => JSX.Element;
export declare const TextBlock2: (props: import("./types/componentDataTypes").TextBlock2Data) => JSX.Element;
