import { ComponentsEnum, ComponentsProps } from "./types";
import { ComponentMap } from "./types/componentDataTypes";
import { FuncMap } from "./types/componentFunctionTypes";
declare function getPropTemplate<T extends ComponentsEnum>(components: T): FuncMap[T];
declare function getProperties<T extends ComponentsEnum>(component: T, props: ComponentMap[T]): ComponentsProps<T>;
export declare const getPropTemplateFunc: typeof getPropTemplate;
export declare const getPropertiesFunc: typeof getProperties;
export {};
