import { FileStackCredentials, StyleProps } from "./index";
declare type DisabledProp = {
    disabled?: boolean;
};
export declare type TextBlock1Data = DisabledProp & StyleProps & {
    text1: string;
};
export declare type TextBlock2Data = DisabledProp & StyleProps & TextBlock1Data & {
    text2: string;
};
export declare type TextBlock3Data = DisabledProp & StyleProps & TextBlock2Data & {
    text3: string;
};
export declare type Headline1Data = DisabledProp & StyleProps & {
    headline1: string;
};
export declare type Headline2Data = DisabledProp & StyleProps & Headline1Data & {
    headline2: string;
};
export declare type Headline3Data = DisabledProp & StyleProps & Headline2Data & {
    headline3: string;
};
export declare type EyebrownData = DisabledProp & StyleProps & {
    eyebrown: string;
};
export declare type HeadlineWithTeaserData = DisabledProp & StyleProps & Headline1Data & TextBlock1Data;
export declare type HeadlineWithTeaser2Data = HeadlineWithTeaserData;
export declare type H3WithTextData = HeadlineWithTeaserData;
export declare type PictureData = DisabledProp & StyleProps & TextBlock1Data & {
    picture: string;
    fileStack: FileStackCredentials;
};
export declare type Picture2Data = DisabledProp & StyleProps & PictureData & TextBlock2Data & {
    picture2: string;
};
export declare type Picture3Data = DisabledProp & StyleProps & Picture2Data & TextBlock3Data & {
    picture3: string;
};
export declare type PictureWithTextData = DisabledProp & StyleProps & PictureData & TextBlock2Data;
export declare type HeadlineEyebrownTextData = DisabledProp & StyleProps & Headline1Data & TextBlock1Data & EyebrownData;
export declare type PictureHeadlineText3Data = DisabledProp & StyleProps & Picture3Data & Headline3Data & TextBlock3Data;
export declare type ComponentMap = {
    headline: Headline1Data;
    headlineEyebrownText: HeadlineEyebrownTextData;
    headlineWithTeaser: HeadlineWithTeaserData;
    headlineWithTeaser2: HeadlineWithTeaser2Data;
    h3WithText: H3WithTextData;
    picture: PictureData;
    picture2: Picture2Data;
    picture3: Picture3Data;
    pictureHeadlineText3: PictureHeadlineText3Data;
    pictureWithText: PictureWithTextData;
    textBlock: TextBlock1Data;
    textBlock2: TextBlock2Data;
};
export {};
