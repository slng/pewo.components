import React, { CSSProperties } from "react";
import { ComponentMap } from "./componentDataTypes";
import { FuncMap } from "./componentFunctionTypes";
export declare type Children = React.ReactChild | React.ReactChildren | React.ReactFragment | React.ReactPortal | boolean | null | undefined;
export declare enum ComponentsEnum {
    headline = "headline",
    headlineEyebrownText = "headlineEyebrownText",
    headlineWithTeaser = "headlineWithTeaser",
    headlineWithTeaser2 = "headlineWithTeaser2",
    h3WithText = "h3WithText",
    picture = "picture",
    picture2 = "picture2",
    picture3 = "picture3",
    pictureHeadlineText3 = "pictureHeadlineText3",
    pictureWithText = "pictureWithText",
    textBlock = "textBlock",
    textBlock2 = "textBlock2"
}
export declare type ComponentsFunctions<T extends ComponentsEnum> = FuncMap[T];
export declare type StyleProps = {
    className?: string;
    style?: CSSProperties;
};
export declare type ComponentsProps<T extends ComponentsEnum> = StyleProps & {
    type: T;
    props: ComponentMap[T];
    componentObj: {
        modified: boolean;
    };
};
export declare type FileStackCredentials = {
    api: string;
    policy: string;
    signature: string;
};
export declare type fileStackOnPick = {
    onPick: any;
};
