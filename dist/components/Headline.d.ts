/// <reference types="react" />
import { Headline1Data } from "../types/componentDataTypes";
declare const Headline: (props: Headline1Data) => JSX.Element;
export default Headline;
