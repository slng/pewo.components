/// <reference types="react" />
import { PictureWithTextData } from "../types/componentDataTypes";
declare const PictureWithText: (props: PictureWithTextData) => JSX.Element;
export default PictureWithText;
