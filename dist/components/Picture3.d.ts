/// <reference types="react" />
import { Picture3Data } from "../types/componentDataTypes";
declare const Picture3: (props: Picture3Data) => JSX.Element;
export default Picture3;
