/// <reference types="react" />
import { TextBlock2Data } from "../types/componentDataTypes";
declare const TextBlock2: (props: TextBlock2Data) => JSX.Element;
export default TextBlock2;
