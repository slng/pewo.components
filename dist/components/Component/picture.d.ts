/// <reference types="react" />
import { ComponentsEnum, ComponentsProps } from "../../types";
declare const Picture: ({ props, componentObj }: ComponentsProps<ComponentsEnum.picture>) => JSX.Element;
export default Picture;
