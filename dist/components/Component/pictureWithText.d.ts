/// <reference types="react" />
import { ComponentsEnum, ComponentsProps } from "../../types";
declare const PictureWithText: ({ props, componentObj }: ComponentsProps<ComponentsEnum.pictureWithText>) => JSX.Element;
export default PictureWithText;
