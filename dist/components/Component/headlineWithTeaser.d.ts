/// <reference types="react" />
import { ComponentsEnum, ComponentsProps } from "../../types";
declare const HeadLineWithTeaser: ({ props, componentObj }: ComponentsProps<ComponentsEnum.headlineWithTeaser>) => JSX.Element;
export default HeadLineWithTeaser;
