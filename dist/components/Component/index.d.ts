/// <reference types="react" />
import { ComponentsEnum, ComponentsProps } from "../../types";
declare const Component: (props: ComponentsProps<ComponentsEnum>) => JSX.Element;
export default Component;
