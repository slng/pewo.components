/// <reference types="react" />
import { ComponentsEnum, ComponentsProps } from "../../types";
declare const TextBlock: ({ props, componentObj }: ComponentsProps<ComponentsEnum.textBlock>) => JSX.Element;
export default TextBlock;
