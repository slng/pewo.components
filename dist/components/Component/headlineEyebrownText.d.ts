/// <reference types="react" />
import { ComponentsEnum, ComponentsProps } from "../../types";
declare const HeadlineEyebrownText: ({ props, componentObj }: ComponentsProps<ComponentsEnum.headlineEyebrownText>) => JSX.Element;
export default HeadlineEyebrownText;
