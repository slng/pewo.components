/// <reference types="react" />
import { ComponentsEnum, ComponentsProps } from "../../types";
declare const HeadLineWithTeaser2: ({ props, componentObj }: ComponentsProps<ComponentsEnum.headlineWithTeaser2>) => JSX.Element;
export default HeadLineWithTeaser2;
