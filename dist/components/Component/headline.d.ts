/// <reference types="react" />
import { ComponentsEnum, ComponentsProps } from "../../types";
declare const HeadLine: ({ props, componentObj }: ComponentsProps<ComponentsEnum.headline>) => JSX.Element;
export default HeadLine;
