/// <reference types="react" />
import { ComponentsEnum, ComponentsProps } from "../../types";
declare const H2WithText: ({ props, componentObj }: ComponentsProps<ComponentsEnum.headlineWithTeaser>) => JSX.Element;
export default H2WithText;
