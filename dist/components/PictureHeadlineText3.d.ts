/// <reference types="react" />
import { PictureHeadlineText3Data } from "../types/componentDataTypes";
declare const PictureHeadlineText3: (props: PictureHeadlineText3Data) => JSX.Element;
export default PictureHeadlineText3;
