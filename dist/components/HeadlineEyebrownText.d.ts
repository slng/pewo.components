/// <reference types="react" />
import { HeadlineEyebrownTextData } from "../types/componentDataTypes";
declare const HeadlineEyebrownText: (props: HeadlineEyebrownTextData) => JSX.Element;
export default HeadlineEyebrownText;
