/// <reference types="react" />
import { HeadlineWithTeaserData } from "../types/componentDataTypes";
declare const HeadlineWithTeaser: (props: HeadlineWithTeaserData) => JSX.Element;
export default HeadlineWithTeaser;
