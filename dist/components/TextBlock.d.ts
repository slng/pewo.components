/// <reference types="react" />
import { TextBlock1Data } from "../types/componentDataTypes";
declare const TextBlock: (props: TextBlock1Data) => JSX.Element;
export default TextBlock;
