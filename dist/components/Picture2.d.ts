/// <reference types="react" />
import { Picture2Data } from "../types/componentDataTypes";
declare const Picture2: (props: Picture2Data) => JSX.Element;
export default Picture2;
