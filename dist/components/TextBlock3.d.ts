/// <reference types="react" />
import { TextBlock3Data } from "../types/componentDataTypes";
declare const TextBlock3: (props: TextBlock3Data) => JSX.Element;
export default TextBlock3;
