/// <reference types="react" />
import { PictureData } from "../types/componentDataTypes";
declare const Picture: (props: PictureData) => JSX.Element;
export default Picture;
