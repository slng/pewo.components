import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { TextBlock2Data } from "../types/componentDataTypes";
import TextBlock2Component from "./Component/textBlock2";

const TextBlock2 = (props: TextBlock2Data) => {
  return (
    <TextBlock2Component
      {...getPropertiesFunc<ComponentsEnum.textBlock2>(
        ComponentsEnum.textBlock2,
        props
      )}
    />
  );
};

export default TextBlock2;
