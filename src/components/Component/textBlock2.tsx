import React from "react";
import ContentEditable from "react-contenteditable";
import themeStyles from "../../assets/css/theme.min.css";
import { ComponentsEnum, ComponentsProps } from "../../types";

const TextBlock2 = ({
  props,
  componentObj
}: ComponentsProps<ComponentsEnum.textBlock2>) => {
  const { style, className, disabled } = props;
  const text1 =
    props.text1 ||
    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";

  const text2 =
    props.text2 ||
    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";

  return (
    <section className={className} style={style}>
      <div className={themeStyles.container}>
        <div className={themeStyles.row}>
          <div className={themeStyles.col6}>
            <ContentEditable
              html={text1}
              disabled={disabled !== undefined ? disabled : true}
              style={disabled !== undefined ? { cursor: "text" } : {}}
              onChange={e => {
                props.text1 = e.target.value;
                componentObj.modified = true;
              }}
            />
          </div>
          <div className={themeStyles.col6}>
            <ContentEditable
              html={text2}
              disabled={disabled !== undefined ? disabled : true}
              style={disabled !== undefined ? { cursor: "text" } : {}}
              onChange={e => {
                props.text2 = e.target.value;
                componentObj.modified = true;
              }}
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default TextBlock2;
