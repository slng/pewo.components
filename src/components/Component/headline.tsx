import React from "react";
import ContentEditable from "react-contenteditable";
import themeStyles from "../../assets/css/theme.min.css";
import { ComponentsEnum, ComponentsProps } from "../../types";

const HeadLine = ({
  props,
  componentObj
}: ComponentsProps<ComponentsEnum.headline>) => {
  const { style, className, disabled } = props;
  const html = props.headline1 || "Example Headline";

  return (
    <section className={className} style={style}>
      <div className={themeStyles.container}>
        <div className={themeStyles.row}>
          <div className={themeStyles.col12}>
            <ContentEditable
              html={html}
              disabled={disabled !== undefined ? disabled : true}
              tagName="h1"
              style={disabled !== undefined ? { cursor: "text" } : {}}
              onChange={e => {
                props.headline1 = e.target.value;
                componentObj.modified = true;
              }}
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default HeadLine;
