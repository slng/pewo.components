import React from "react";
import ContentEditable from "react-contenteditable";
import themeStyles from "../../assets/css/theme.min.css";
import { ComponentsEnum, ComponentsProps } from "../../types";

const TextBlock = ({
  props,
  componentObj
}: ComponentsProps<ComponentsEnum.textBlock>) => {
  const { style, className, disabled } = props;
  const html =
    props.text1 ||
    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";

  return (
    <section className={className} style={style}>
      <div className={themeStyles.container}>
        <div className={themeStyles.row}>
          <div className={themeStyles.col12}>
            <ContentEditable
              html={html}
              disabled={disabled}
              style={disabled ? {} : { cursor: "text" }}
              onChange={e => {
                props.text1 = e.target.value;
                componentObj.modified = true;
              }}
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default TextBlock;
