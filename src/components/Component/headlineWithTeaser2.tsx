import React from "react";
import ContentEditable from "react-contenteditable";
import themeStyles from "../../assets/css/theme.min.css";
import pageStyles from "../../styles/pages.css";
import { ComponentsEnum, ComponentsProps } from "../../types";

const HeadLineWithTeaser2 = ({
  props,
  componentObj
}: ComponentsProps<ComponentsEnum.headlineWithTeaser2>) => {
  const { style, className, disabled } = props;
  const headline = props.headline1 || "Headline not loaded";
  const textBlock1 = props.text1 || "Textblock not loaded";

  return (
    <section className={className} style={style}>
      <div className={themeStyles.container}>
        <div className={themeStyles.row}>
          <ContentEditable
            html={headline}
            className={themeStyles.col12}
            disabled={disabled}
            tagName="h1"
            style={disabled ? {} : { cursor: "text" }}
            onChange={e => {
              props.headline1 = e.target.value;
              componentObj.modified = true;
            }}
          />
          <ContentEditable
            html={textBlock1}
            className={`${themeStyles.col12} ${pageStyles.colFloat2}`}
            disabled={disabled}
            style={disabled !== undefined ? { cursor: "text" } : {}}
            onChange={e => {
              props.text1 = e.target.value;
              componentObj.modified = true;
            }}
          />
        </div>
      </div>
    </section>
  );
};

export default HeadLineWithTeaser2;
