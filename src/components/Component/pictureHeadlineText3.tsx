import loadable from "@loadable/component";
import React, { useCallback, useState } from "react";
import ContentEditable from "react-contenteditable";
import themeStyles from "../../assets/css/theme.min.css";
import { ComponentsEnum, ComponentsProps, fileStackOnPick } from "../../types";

const ReactFileStack = loadable(() => import("filestack-react"), {
  ssr: false
});

const PictureHeadlineText3 = ({
  props,
  componentObj
}: ComponentsProps<ComponentsEnum.pictureHeadlineText3>) => {
  const { style, className, disabled } = props;
  const [fileHandle1, setFileHandle1] = useState<string>(
    props.picture || "https://via.placeholder.com/800x200"
  );
  const [fileHandle2, setFileHandle2] = useState<string>(
    props.picture2 || "https://via.placeholder.com/800x200"
  );
  const [fileHandle3, setFileHandle3] = useState<string>(
    props.picture3 || "https://via.placeholder.com/800x200"
  );

  const handleFileAdd1 = useCallback(
    (result: any) => {
      setFileHandle1(result.filesUploaded[0].handle);
      props.picture = result.filesUploaded[0].handle;
      componentObj.modified = true;
    },
    [setFileHandle1]
  );

  const handleFileAdd2 = useCallback(
    (result: any) => {
      setFileHandle2(result.filesUploaded[0].handle);
      props.picture2 = result.filesUploaded[0].handle;
      componentObj.modified = true;
    },
    [setFileHandle2]
  );

  const handleFileAdd3 = useCallback(
    (result: any) => {
      setFileHandle3(result.filesUploaded[0].handle);
      props.picture3 = result.filesUploaded[0].handle;
      componentObj.modified = true;
    },
    [setFileHandle2]
  );

  const {
    fileStack: { api, policy, signature }
  } = props;

  const headline1 = props.headline1 || "Picture: test title";
  const headline2 = props.headline2 || "Picture: test title";
  const headline3 = props.headline3 || "Picture: test title";

  const text1 = props.text1 || "Picture: test text";
  const text2 = props.text2 || "Picture: test text";
  const text3 = props.text3 || "Picture: test text";

  return (
    <section className={className} style={style}>
      <div className={themeStyles.container}>
        <div className={themeStyles.row}>
          <div className={`${themeStyles.col12} ${themeStyles.colMd4}`}>
            <ReactFileStack
              apikey={api}
              action="pick"
              clientOptions={{
                security: {
                  policy,
                  signature
                }
              }}
              disabled={disabled}
              onSuccess={handleFileAdd1}
              customRender={({ onPick }: fileStackOnPick) => (
                <img
                  id={"picture"}
                  className={`${themeStyles.rounded} ${themeStyles.border}`}
                  style={{ maxWidth: "100%", maxHeight: "100%" }}
                  src={
                    fileHandle1 !== ""
                      ? `https://cdn.filestackcontent.com/${api}/security=policy:${policy},signature:${signature}/output=f:png/${fileHandle1}`
                      : `https://via.placeholder.com/110.png?text=TestText`
                  }
                  alt=""
                  onClick={e => {
                    if (!disabled) {
                      onPick(e);
                    }
                  }}
                />
              )}
            />
            <ContentEditable
              html={headline1}
              tagName="h3"
              onChange={e => {
                props.headline1 = e.target.value;
                componentObj.modified = true;
              }}
              disabled={disabled}
            />
            <ContentEditable
              html={text1}
              tagName="p"
              className={`${themeStyles.textMuted} ${themeStyles.mb6} ${themeStyles.mbMd0}`}
              onChange={e => {
                props.text1 = e.target.value;
                componentObj.modified = true;
              }}
              disabled={disabled}
            />
          </div>
          <div className={`${themeStyles.col12} ${themeStyles.colMd4}`}>
            <ReactFileStack
              apikey={api}
              action="pick"
              clientOptions={{
                security: {
                  policy,
                  signature
                }
              }}
              disabled={disabled}
              onSuccess={handleFileAdd2}
              customRender={({ onPick }: fileStackOnPick) => (
                <img
                  id={"picture"}
                  className={`${themeStyles.rounded} ${themeStyles.border}`}
                  style={{ maxWidth: "100%", maxHeight: "100%" }}
                  src={
                    fileHandle1 !== ""
                      ? `https://cdn.filestackcontent.com/${api}/security=policy:${policy},signature:${signature}/output=f:png/${fileHandle2}`
                      : `https://via.placeholder.com/110.png?text=TestText`
                  }
                  alt=""
                  onClick={e => {
                    if (!disabled) {
                      onPick(e);
                    }
                  }}
                />
              )}
            />
            <ContentEditable
              html={headline2}
              tagName="h3"
              onChange={e => {
                props.headline2 = e.target.value;
                componentObj.modified = true;
              }}
              disabled={disabled}
            />
            <ContentEditable
              html={text2}
              tagName="p"
              className={`${themeStyles.textMuted} ${themeStyles.mb6} ${themeStyles.mbMd0}`}
              onChange={e => {
                props.text2 = e.target.value;
                componentObj.modified = true;
              }}
              disabled={disabled}
            />
          </div>
          <div className={`${themeStyles.col12} ${themeStyles.colMd4}`}>
            <ReactFileStack
              apikey={api}
              action="pick"
              clientOptions={{
                security: {
                  policy,
                  signature
                }
              }}
              disabled={disabled}
              onSuccess={handleFileAdd3}
              customRender={({ onPick }: fileStackOnPick) => (
                <img
                  id={"picture"}
                  className={`${themeStyles.rounded} ${themeStyles.border}`}
                  style={{ maxWidth: "100%", maxHeight: "100%" }}
                  src={
                    fileHandle1 !== ""
                      ? `https://cdn.filestackcontent.com/${api}/security=policy:${policy},signature:${signature}/output=f:png/${fileHandle3}`
                      : `https://via.placeholder.com/110.png?text=TestText`
                  }
                  alt=""
                  onClick={e => {
                    if (!disabled) {
                      onPick(e);
                    }
                  }}
                />
              )}
            />
            <ContentEditable
              html={headline3}
              tagName="h3"
              onChange={e => {
                props.headline3 = e.target.value;
                componentObj.modified = true;
              }}
              disabled={disabled}
            />
            <ContentEditable
              html={text3}
              tagName="p"
              className={`${themeStyles.textMuted} ${themeStyles.mb6} ${themeStyles.mbMd0}`}
              onChange={e => {
                props.text3 = e.target.value;
                componentObj.modified = true;
              }}
              disabled={disabled}
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default PictureHeadlineText3;
