import React from "react";
import ContentEditable from "react-contenteditable";
import themeStyles from "../../assets/css/theme.min.css";
import pageStyles from "../../styles/pages.css";
import { ComponentsEnum, ComponentsProps } from "../../types";

const HeadlineEyebrownText = ({
  props,
  componentObj
}: ComponentsProps<ComponentsEnum.headlineEyebrownText>) => {
  const { style, className, disabled } = props;
  const headline = props.headline1 || "Headline not loaded";
  const textBlock = props.text1 || "Textblock not loaded";
  const eyebrown = props.eyebrown || "Eyebrown not loaded";

  return (
    <section className={className} style={style}>
      <div className={`${themeStyles.container}`}>
        <div className={`${themeStyles.row}`}>
          <div className={`${themeStyles.col12} ${themeStyles.colMd12}`}>
            <ContentEditable
              html={eyebrown}
              disabled={disabled}
              tagName="h6"
              style={disabled !== undefined ? { cursor: "text" } : {}}
              className={`${themeStyles.textUppercase} ${pageStyles.textPrimary} ${themeStyles.fontWeightBold}`}
              onChange={e => {
                props.eyebrown = e.target.value;
                componentObj.modified = true;
              }}
            />
            <ContentEditable
              html={headline}
              disabled={disabled}
              tagName="h2"
              style={disabled !== undefined ? { cursor: "text" } : {}}
              onChange={e => {
                props.headline1 = e.target.value;
                componentObj.modified = true;
              }}
            />
            <ContentEditable
              html={textBlock}
              disabled={disabled}
              tagName="p"
              style={disabled !== undefined ? { cursor: "text" } : {}}
              className={`${themeStyles.textMuted} ${themeStyles.mb6} ${themeStyles.mbMd8}`}
              onChange={e => {
                props.text1 = e.target.value;
                componentObj.modified = true;
              }}
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default HeadlineEyebrownText;
