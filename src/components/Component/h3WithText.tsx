import React from "react";
import ContentEditable from "react-contenteditable";
import themeStyles from "../../assets/css/theme.min.css";
import { ComponentsEnum, ComponentsProps } from "../../types";

const H2WithText = ({
  props,
  componentObj
}: ComponentsProps<ComponentsEnum.h3WithText>) => {
  const { className, disabled } = props;
  const headline = props.headline1 || "Example Headline";
  const text = props.text1 || "Example Fliesstext";

  return (
    <section className={className}>
      <div className={themeStyles.container}>
        <div className={themeStyles.row}>
          <div className={themeStyles.col12}>
            <ContentEditable
              html={headline}
              disabled={disabled !== undefined ? disabled : true}
              tagName="h3"
              style={disabled !== undefined ? { cursor: "text" } : {}}
              onChange={e => {
                props.headline1 = e.target.value;
                componentObj.modified = true;
              }}
            />
            <ContentEditable
              html={text}
              disabled={disabled !== undefined ? disabled : true}
              tagName="div"
              style={disabled !== undefined ? { cursor: "text" } : {}}
              onChange={e => {
                props.text1 = e.target.value;
                componentObj.modified = true;
              }}
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default H2WithText;
