import loadable from "@loadable/component";
import React, { useCallback, useState } from "react";
import ContentEditable from "react-contenteditable";
import themeStyles from "../../assets/css/theme.min.css";
import { ComponentsEnum, ComponentsProps, fileStackOnPick } from "../../types";

const ReactFileStack = loadable(() => import("filestack-react"), {
  ssr: false
});

const PictureWithText = ({
  props,
  componentObj
}: ComponentsProps<ComponentsEnum.pictureWithText>) => {
  const { style, className, disabled } = props;
  const [fileHandle, setFilestackHandle] = useState<string>(
    props.picture || "https://via.placeholder.com/800x200"
  );

  const handleFileAdd = useCallback(
    (result: any) => {
      console.log(result);
      setFilestackHandle(result.filesUploaded[0].handle);
      props.picture = result.filesUploaded[0].handle;
      componentObj.modified = true;
    },
    [setFilestackHandle]
  );

  const {
    text1,
    text2,
    fileStack: { api, policy, signature }
  } = props;

  const subtitle = text1 || "Picture: test subtitle";
  const text = text2 || "Test text .......";

  return (
    <section className={className} style={style}>
      <div className={themeStyles.container}>
        <div className={themeStyles.row}>
          <div className={themeStyles.col6}>
            <figure className={`${themeStyles.textCenter} ${themeStyles.p2}`}>
              <ReactFileStack
                apikey={api}
                action="pick"
                clientOptions={{
                  security: {
                    policy,
                    signature
                  }
                }}
                disabled={disabled}
                onSuccess={handleFileAdd}
                customRender={({ onPick }: fileStackOnPick) => (
                  <img
                    id="picture"
                    className={`${themeStyles.rounded} ${themeStyles.border}`}
                    style={{ maxWidth: "100%", maxHeight: "100%" }}
                    src={
                      fileHandle !== ""
                        ? `https://cdn.filestackcontent.com/${api}/security=policy:${policy},signature:${signature}/output=f:png/${fileHandle}`
                        : `https://via.placeholder.com/110.png?text=TestText`
                    }
                    alt=""
                    onClick={e => {
                      if (!disabled) {
                        onPick(e);
                      }
                    }}
                  />
                )}
              />
              <ContentEditable
                html={subtitle}
                tagName="figcaption"
                className={themeStyles.small}
                onChange={e => {
                  props.text1 = e.target.value;
                  componentObj.modified = true;
                }}
                disabled={disabled}
              />
            </figure>
          </div>
          <ContentEditable
            html={text}
            className={themeStyles.col6}
            disabled={disabled}
            onChange={e => {
              props.text2 = e.target.value;
              componentObj.modified = true;
            }}
          />
        </div>
      </div>
    </section>
  );
};

export default PictureWithText;
