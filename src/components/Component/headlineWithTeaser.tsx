import React from "react";
import ContentEditable from "react-contenteditable";
import themeStyles from "../../assets/css/theme.min.css";
import { ComponentsEnum, ComponentsProps } from "../../types";

const HeadLineWithTeaser = ({
  props,
  componentObj
}: ComponentsProps<ComponentsEnum.headlineWithTeaser>) => {
  const { style, className, disabled } = props;
  const headline = props.headline1 || "Headline not loaded";
  const textBlock = props.text1 || "Textblock not loaded";

  return (
    <section className={className} style={style}>
      ergearwg
      <div className={themeStyles.container}>
        <div className={`${themeStyles.row} ${themeStyles.noGutters}`}>
          <div className={themeStyles.col12}>
            <ContentEditable
              html={headline}
              disabled={disabled}
              tagName="h1"
              style={disabled !== undefined ? { cursor: "text" } : {}}
              onChange={e => {
                props.headline1 = e.target.value;
                componentObj.modified = true;
              }}
            />

            <ContentEditable
              html={textBlock}
              disabled={disabled !== undefined ? disabled : true}
              style={disabled !== undefined ? { cursor: "text" } : {}}
              onChange={e => {
                props.text1 = e.target.value;
                componentObj.modified = true;
              }}
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default HeadLineWithTeaser;
