import React from "react";
import { ComponentsEnum, ComponentsProps } from "../../types";
import H3WithText from "./h3WithText";
import HeadLine from "./headline";
import HeadlineEyebrownText from "./headlineEyebrownText";
import HeadLineWithTeaser from "./headlineWithTeaser";
import HeadLineWithTeaser2 from "./headlineWithTeaser2";
import Picture from "./picture";
import Picture2 from "./picture2";
import Picture3 from "./picture3";
import PictureHeadlineText3 from "./pictureHeadlineText3";
import PictureWithText from "./pictureWithText";
import TextBlock from "./textBlock";
import TextBlock2 from "./textBlock2";

const Component = (props: ComponentsProps<ComponentsEnum>) => {
  switch (props.type) {
    case ComponentsEnum.headline: {
      return (
        <HeadLine {...(props as ComponentsProps<ComponentsEnum.headline>)} />
      );
    }
    case ComponentsEnum.headlineEyebrownText: {
      return (
        <HeadlineEyebrownText
          {...(props as ComponentsProps<ComponentsEnum.headlineEyebrownText>)}
        />
      );
    }
    case ComponentsEnum.headlineWithTeaser: {
      return (
        <HeadLineWithTeaser
          {...(props as ComponentsProps<ComponentsEnum.headlineWithTeaser>)}
        />
      );
    }
    case ComponentsEnum.headlineWithTeaser2: {
      return (
        <HeadLineWithTeaser2
          {...(props as ComponentsProps<ComponentsEnum.headlineWithTeaser2>)}
        />
      );
    }
    case ComponentsEnum.h3WithText: {
      return (
        <H3WithText
          {...(props as ComponentsProps<ComponentsEnum.h3WithText>)}
        />
      );
    }
    case ComponentsEnum.picture: {
      return (
        <Picture {...(props as ComponentsProps<ComponentsEnum.picture>)} />
      );
    }
    case ComponentsEnum.picture2: {
      return (
        <Picture2 {...(props as ComponentsProps<ComponentsEnum.picture2>)} />
      );
    }
    case ComponentsEnum.picture3: {
      return (
        <Picture3 {...(props as ComponentsProps<ComponentsEnum.picture3>)} />
      );
    }
    case ComponentsEnum.pictureHeadlineText3: {
      return (
        <PictureHeadlineText3
          {...(props as ComponentsProps<ComponentsEnum.pictureHeadlineText3>)}
        />
      );
    }
    case ComponentsEnum.pictureWithText: {
      return (
        <PictureWithText
          {...(props as ComponentsProps<ComponentsEnum.pictureWithText>)}
        />
      );
    }
    case ComponentsEnum.textBlock: {
      return (
        <TextBlock {...(props as ComponentsProps<ComponentsEnum.textBlock>)} />
      );
    }
    case ComponentsEnum.textBlock2: {
      return (
        <TextBlock2
          {...(props as ComponentsProps<ComponentsEnum.textBlock2>)}
        />
      );
    }
  }
};

export default Component;
