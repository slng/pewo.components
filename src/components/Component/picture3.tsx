import loadable from "@loadable/component";
import React, { useCallback, useState } from "react";
import ContentEditable from "react-contenteditable";
import themeStyles from "../../assets/css/theme.min.css";
import { ComponentsEnum, ComponentsProps, fileStackOnPick } from "../../types";

const ReactFileStack = loadable(() => import("filestack-react"), {
  ssr: false
});

const Picture3 = ({
  props,
  componentObj
}: ComponentsProps<ComponentsEnum.picture3>) => {
  const { style, className, disabled } = props;
  const [fileHandle1, setFileHandle1] = useState<string>(
    props.picture || "https://via.placeholder.com/800x200"
  );
  const [fileHandle2, setFileHandle2] = useState<string>(
    props.picture2 || "https://via.placeholder.com/800x200"
  );
  const [fileHandle3, setFileHandle3] = useState<string>(
    props.picture3 || "https://via.placeholder.com/800x200"
  );

  const handleFileAdd1 = useCallback(
    (result: any) => {
      setFileHandle1(result.filesUploaded[0].handle);
      props.picture = result.filesUploaded[0].handle;
      componentObj.modified = true;
    },
    [setFileHandle1]
  );

  const handleFileAdd2 = useCallback(
    (result: any) => {
      setFileHandle2(result.filesUploaded[0].handle);
      props.picture2 = result.filesUploaded[0].handle;
      componentObj.modified = true;
    },
    [setFileHandle2]
  );

  const handleFileAdd3 = useCallback(
    (result: any) => {
      setFileHandle3(result.filesUploaded[0].handle);
      props.picture3 = result.filesUploaded[0].handle;
      componentObj.modified = true;
    },
    [setFileHandle2]
  );

  const {
    text1,
    text2,
    text3,
    fileStack: { api, policy, signature }
  } = props;

  const subtitle1 = text1 || "Picture: test subtitle";
  const subtitle2 = text2 || "Picture: test subtitle";
  const subtitle3 = text3 || "Picture: test subtitle";

  return (
    <section className={className} style={style}>
      <div className={themeStyles.container}>
        <div className={themeStyles.row}>
          <div className={themeStyles.col4}>
            <figure className={`${themeStyles.textCenter} ${themeStyles.p2}`}>
              <ReactFileStack
                apikey={api}
                action="pick"
                clientOptions={{
                  security: {
                    policy,
                    signature
                  }
                }}
                disabled={disabled}
                onSuccess={handleFileAdd1}
                customRender={({ onPick }: fileStackOnPick) => (
                  <img
                    id={"picture"}
                    className={`${themeStyles.rounded} ${themeStyles.border}`}
                    style={{ maxWidth: "100%", maxHeight: "100%" }}
                    src={
                      fileHandle1 !== ""
                        ? `https://cdn.filestackcontent.com/${api}/security=policy:${policy},signature:${signature}/output=f:png/${fileHandle1}`
                        : `https://via.placeholder.com/110.png?text=TestText`
                    }
                    alt=""
                    onClick={e => {
                      if (!disabled) {
                        onPick(e);
                      }
                    }}
                  />
                )}
              />
              <ContentEditable
                html={subtitle1}
                tagName="figcaption"
                className={themeStyles.small}
                onChange={e => {
                  props.text1 = e.target.value;
                  componentObj.modified = true;
                }}
                disabled={disabled}
              />
            </figure>
          </div>
          <div className={themeStyles.col4}>
            <figure className={`${themeStyles.textCenter} ${themeStyles.p2}`}>
              <ReactFileStack
                apikey={api}
                action="pick"
                clientOptions={{
                  security: {
                    policy,
                    signature
                  }
                }}
                disabled={disabled}
                onSuccess={handleFileAdd2}
                customRender={({ onPick }: fileStackOnPick) => (
                  <img
                    id={"picture"}
                    className={`${themeStyles.rounded} ${themeStyles.border}`}
                    style={{ maxWidth: "100%", maxHeight: "100%" }}
                    src={
                      fileHandle2 !== ""
                        ? `https://cdn.filestackcontent.com/${api}/security=policy:${policy},signature:${signature}/output=f:png/${fileHandle2}`
                        : `https://via.placeholder.com/110.png?text=TestText`
                    }
                    alt=""
                    onClick={e => {
                      if (!disabled) {
                        onPick(e);
                      }
                    }}
                  />
                )}
              />
              <ContentEditable
                html={subtitle2}
                tagName="figcaption"
                className={themeStyles.small}
                onChange={e => {
                  props.text2 = e.target.value;
                  componentObj.modified = true;
                }}
                disabled={disabled}
              />
            </figure>
          </div>
          <div className={themeStyles.col4}>
            <figure className={`${themeStyles.textCenter} ${themeStyles.p2}`}>
              <ReactFileStack
                apikey={api}
                action="pick"
                clientOptions={{
                  security: {
                    policy,
                    signature
                  }
                }}
                disabled={disabled}
                onSuccess={handleFileAdd3}
                customRender={({ onPick }: fileStackOnPick) => (
                  <img
                    id={"picture"}
                    className={`${themeStyles.rounded} ${themeStyles.border}`}
                    style={{ maxWidth: "100%", maxHeight: "100%" }}
                    src={
                      fileHandle3 !== ""
                        ? `https://cdn.filestackcontent.com/${api}/security=policy:${policy},signature:${signature}/output=f:png/${fileHandle3}`
                        : `https://via.placeholder.com/110.png?text=TestText`
                    }
                    alt=""
                    onClick={e => {
                      if (!disabled) {
                        onPick(e);
                      }
                    }}
                  />
                )}
              />
              <ContentEditable
                html={subtitle3}
                tagName="figcaption"
                className={themeStyles.small}
                onChange={() => {
                  componentObj.modified = true;
                }}
                disabled={disabled}
              />
            </figure>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Picture3;
