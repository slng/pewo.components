import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { HeadlineWithTeaser2Data } from "../types/componentDataTypes";
import HeadlineWithTeaser2Component from "./Component/headlineWithTeaser2";

const HeadlineWithTeaser2 = (props: HeadlineWithTeaser2Data) => {
  return (
    <HeadlineWithTeaser2Component
      {...getPropertiesFunc<ComponentsEnum.headlineWithTeaser2>(
        ComponentsEnum.headlineWithTeaser2,
        props
      )}
    />
  );
};

export default HeadlineWithTeaser2;
