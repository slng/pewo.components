import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { HeadlineEyebrownTextData } from "../types/componentDataTypes";
import HeadlineEyebrownTextComponent from "./Component/headlineEyebrownText";

const HeadlineEyebrownText = (props: HeadlineEyebrownTextData) => {
  return (
    <HeadlineEyebrownTextComponent
      {...getPropertiesFunc<ComponentsEnum.headlineEyebrownText>(
        ComponentsEnum.headlineEyebrownText,
        props
      )}
    />
  );
};

export default HeadlineEyebrownText;
