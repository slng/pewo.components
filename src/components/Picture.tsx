import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { PictureData } from "../types/componentDataTypes";
import PictureComponent from "./Component/picture";

const Picture = (props: PictureData) => {
  return (
    <PictureComponent
      {...getPropertiesFunc<ComponentsEnum.picture>(
        ComponentsEnum.picture,
        props
      )}
    />
  );
};

export default Picture;
