import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { Picture2Data } from "../types/componentDataTypes";
import Picture2Component from "./Component/picture2";

const Picture2 = (props: Picture2Data) => {
  return (
    <Picture2Component
      {...getPropertiesFunc<ComponentsEnum.picture2>(
        ComponentsEnum.picture2,
        props
      )}
    />
  );
};

export default Picture2;
