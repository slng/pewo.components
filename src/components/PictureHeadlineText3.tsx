import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { PictureHeadlineText3Data } from "../types/componentDataTypes";
import PictureHeadlineText3Component from "./Component/pictureHeadlineText3";

const PictureHeadlineText3 = (props: PictureHeadlineText3Data) => {
  return (
    <PictureHeadlineText3Component
      {...getPropertiesFunc<ComponentsEnum.pictureHeadlineText3>(
        ComponentsEnum.pictureHeadlineText3,
        props
      )}
    />
  );
};

export default PictureHeadlineText3;
