import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { HeadlineWithTeaserData } from "../types/componentDataTypes";
import HeadlineWithTeaserComponent from "./Component/headlineWithTeaser";

const HeadlineWithTeaser = (props: HeadlineWithTeaserData) => {
  return (
    <HeadlineWithTeaserComponent
      {...getPropertiesFunc<ComponentsEnum.headlineWithTeaser>(
        ComponentsEnum.headlineWithTeaser,
        props
      )}
    />
  );
};

export default HeadlineWithTeaser;
