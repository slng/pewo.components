import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { Picture3Data } from "../types/componentDataTypes";
import Picture3Component from "./Component/picture3";

const Picture3 = (props: Picture3Data) => {
  return (
    <Picture3Component
      {...getPropertiesFunc<ComponentsEnum.picture3>(
        ComponentsEnum.picture3,
        props
      )}
    />
  );
};

export default Picture3;
