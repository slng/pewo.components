import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { H3WithTextData } from "../types/componentDataTypes";
import H3WithTextComponent from "./Component/h3WithText";

const H3WithText = (props: H3WithTextData) => {
  return (
    <H3WithTextComponent
      {...getPropertiesFunc<ComponentsEnum.h3WithText>(
        ComponentsEnum.h3WithText,
        props
      )}
    />
  );
};

export default H3WithText;
