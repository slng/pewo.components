import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { PictureWithTextData } from "../types/componentDataTypes";
import PictureWithTextComponent from "./Component/pictureWithText";

const PictureWithText = (props: PictureWithTextData) => {
  return (
    <PictureWithTextComponent
      {...getPropertiesFunc<ComponentsEnum.pictureWithText>(
        ComponentsEnum.pictureWithText,
        props
      )}
    />
  );
};

export default PictureWithText;
