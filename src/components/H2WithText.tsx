import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { H3WithTextData } from "../types/componentDataTypes";
import H2Component from "./Component/h2WithText";

const H2WithText = (props: H3WithTextData) => {
  return (
    <H2Component
      {...getPropertiesFunc<ComponentsEnum.h3WithText>(
        ComponentsEnum.h3WithText,
        props
      )}
    />
  );
};

export default H2WithText;
