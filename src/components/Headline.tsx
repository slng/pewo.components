import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { Headline1Data } from "../types/componentDataTypes";
import HeadlineComponent from "./Component/headline";

const Headline = (props: Headline1Data) => {
  return (
    <HeadlineComponent
      {...getPropertiesFunc<ComponentsEnum.headline>(
        ComponentsEnum.headline,
        props
      )}
    />
  );
};

export default Headline;
