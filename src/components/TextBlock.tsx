import React from "react";
import { getPropertiesFunc } from "../getPropTemplateFunc";
import { ComponentsEnum } from "../types";
import { TextBlock1Data } from "../types/componentDataTypes";
import TextBlockComponent from "./Component/textBlock";

const TextBlock = (props: TextBlock1Data) => {
  return (
    <TextBlockComponent
      {...getPropertiesFunc<ComponentsEnum.textBlock>(
        ComponentsEnum.textBlock,
        props
      )}
    />
  );
};

export default TextBlock;
