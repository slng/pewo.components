export const textPrimary: string
export const btnPrimary: string
export const colFloat2: string
export const mobileNavigationContainer: string
export const hero: string
export const heroEyebrow: string
export const heroTagline: string
interface Namespace {
	"textPrimary": string,
	"text-primary": string,
	"btnPrimary": string,
	"btn-primary": string,
	"colFloat2": string,
	"mobileNavigationContainer": string,
	"mobile-navigation-container": string,
	"hero": string,
	"heroEyebrow": string,
	"hero-eyebrow": string,
	"heroTagline": string,
	"hero-tagline": string,
}
declare const pages: Namespace
export default pages
