import { ComponentsEnum, ComponentsProps } from "./index";

export type headlineFuncType = (
  headline: string
) => ComponentsProps<ComponentsEnum.headline>;

export type headlineWithTeaserFuncType = (
  headline: string,
  text1: string
) => ComponentsProps<ComponentsEnum.headlineWithTeaser>;

export type headlineWithTeaser2FuncType = (
  headline: string,
  text1: string
) => ComponentsProps<ComponentsEnum.headlineWithTeaser2>;

export type h3WithTextFuncType = (
  headline: string,
  text1: string
) => ComponentsProps<ComponentsEnum.h3WithText>;

export type picturesFuncType = (
  picture: string,
  text1: string
) => ComponentsProps<ComponentsEnum.picture>;

export type pictures2FuncType = (
  picture: string,
  picture2: string,
  text1: string,
  text2: string
) => ComponentsProps<ComponentsEnum.picture2>;

export type pictures3FuncType = (
  picture: string,
  picture2: string,
  picture3: string,
  text1: string,
  text2: string,
  text3: string
) => ComponentsProps<ComponentsEnum.picture3>;

export type picturesHeadlineText3FuncType = (
  picture: string,
  picture2: string,
  picture3: string,
  headline1: string,
  headline2: string,
  headline3: string,
  text1: string,
  text2: string,
  text3: string
) => ComponentsProps<ComponentsEnum.pictureHeadlineText3>;

export type pictureWithTextFuncType = (
  picture: string,
  text1: string,
  text2: string
) => ComponentsProps<ComponentsEnum.pictureWithText>;

export type textBlockFuncType = (
  text1: string
) => ComponentsProps<ComponentsEnum.textBlock>;

export type textBlock2FuncType = (
  text1: string,
  text2: string
) => ComponentsProps<ComponentsEnum.textBlock2>;
export type headlineEyebrownTextFuncType = (
  eyebrown: string,
  text1: string,
  headline: string
) => ComponentsProps<ComponentsEnum.headlineEyebrownText>;

export type FuncMap = {
  headline: headlineFuncType;
  headlineEyebrownText: headlineEyebrownTextFuncType;
  headlineWithTeaser: headlineWithTeaserFuncType;
  headlineWithTeaser2: headlineWithTeaser2FuncType;
  h3WithText: h3WithTextFuncType;
  picture: picturesFuncType;
  picture2: pictures2FuncType;
  picture3: pictures3FuncType;
  pictureHeadlineText3: picturesHeadlineText3FuncType;
  pictureWithText: pictureWithTextFuncType;
  textBlock: textBlockFuncType;
  textBlock2: textBlock2FuncType;
};
