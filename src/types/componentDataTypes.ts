import { FileStackCredentials, StyleProps } from "./index";

type DisabledProp = {
  disabled?: boolean;
};

export type TextBlock1Data = DisabledProp &
  StyleProps & {
    text1: string;
  };

export type TextBlock2Data = DisabledProp &
  StyleProps &
  TextBlock1Data & {
    text2: string;
  };

export type TextBlock3Data = DisabledProp &
  StyleProps &
  TextBlock2Data & {
    text3: string;
  };

export type Headline1Data = DisabledProp &
  StyleProps & {
    headline1: string;
  };

export type Headline2Data = DisabledProp &
  StyleProps &
  Headline1Data & {
    headline2: string;
  };
export type Headline3Data = DisabledProp &
  StyleProps &
  Headline2Data & {
    headline3: string;
  };

export type EyebrownData = DisabledProp &
  StyleProps & {
    eyebrown: string;
  };

export type HeadlineWithTeaserData = DisabledProp &
  StyleProps &
  Headline1Data &
  TextBlock1Data;
export type HeadlineWithTeaser2Data = HeadlineWithTeaserData;
export type H3WithTextData = HeadlineWithTeaserData;

export type PictureData = DisabledProp &
  StyleProps &
  TextBlock1Data & {
    picture: string;
    fileStack: FileStackCredentials;
  };
export type Picture2Data = DisabledProp &
  StyleProps &
  PictureData &
  TextBlock2Data & {
    picture2: string;
  };
export type Picture3Data = DisabledProp &
  StyleProps &
  Picture2Data &
  TextBlock3Data & {
    picture3: string;
  };

export type PictureWithTextData = DisabledProp &
  StyleProps &
  PictureData &
  TextBlock2Data;

export type HeadlineEyebrownTextData = DisabledProp &
  StyleProps &
  Headline1Data &
  TextBlock1Data &
  EyebrownData;

export type PictureHeadlineText3Data = DisabledProp &
  StyleProps &
  Picture3Data &
  Headline3Data &
  TextBlock3Data;

export type ComponentMap = {
  headline: Headline1Data;
  headlineEyebrownText: HeadlineEyebrownTextData;
  headlineWithTeaser: HeadlineWithTeaserData;
  headlineWithTeaser2: HeadlineWithTeaser2Data;
  h3WithText: H3WithTextData;
  picture: PictureData;
  picture2: Picture2Data;
  picture3: Picture3Data;
  pictureHeadlineText3: PictureHeadlineText3Data;
  pictureWithText: PictureWithTextData;
  textBlock: TextBlock1Data;
  textBlock2: TextBlock2Data;
};
