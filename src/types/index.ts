import React, { CSSProperties } from "react";
import { ComponentMap } from "./componentDataTypes";
import { FuncMap } from "./componentFunctionTypes";

export type Children =
  | React.ReactChild
  | React.ReactChildren
  | React.ReactFragment
  | React.ReactPortal
  | boolean
  | null
  | undefined;

export enum ComponentsEnum {
  headline = "headline",
  headlineEyebrownText = "headlineEyebrownText",
  headlineWithTeaser = "headlineWithTeaser",
  headlineWithTeaser2 = "headlineWithTeaser2",
  h3WithText = "h3WithText",
  picture = "picture",
  picture2 = "picture2",
  picture3 = "picture3",
  pictureHeadlineText3 = "pictureHeadlineText3",
  pictureWithText = "pictureWithText",
  textBlock = "textBlock",
  textBlock2 = "textBlock2"
}

export type ComponentsFunctions<T extends ComponentsEnum> = FuncMap[T];

export type StyleProps = {
  className?: string;
  style?: CSSProperties;
};

export type ComponentsProps<T extends ComponentsEnum> = StyleProps & {
  type: T;
  props: ComponentMap[T];
  componentObj: {
    modified: boolean;
  };
};

export type FileStackCredentials = {
  api: string;
  policy: string;
  signature: string;
};

export type fileStackOnPick = { onPick: any };
