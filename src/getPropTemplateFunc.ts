import { ComponentsEnum, ComponentsProps, FileStackCredentials } from "./types";
import { ComponentMap } from "./types/componentDataTypes";
import { FuncMap } from "./types/componentFunctionTypes";

function setFunc<T extends ComponentsEnum>(
  components: T,
  props: ComponentMap[T]
): ComponentsProps<T>;

function setFunc(components: ComponentsEnum, props: ComponentMap) {
  return {
    type: components,
    props,
    disabled: false,
    componentObj: {
      modified: true
    }
  };
}

function getPropTemplate<T extends ComponentsEnum>(components: T): FuncMap[T];
function getPropTemplate(components: ComponentsEnum) {
  switch (ComponentsEnum[components]) {
    case ComponentsEnum.headline: {
      return (headline1: string) =>
        setFunc<ComponentsEnum.headline>(ComponentsEnum.headline, {
          headline1
        });
    }
    case ComponentsEnum.headlineEyebrownText: {
      return (headline1: string, text1: string, eyebrown: string) =>
        setFunc<ComponentsEnum.headlineEyebrownText>(
          ComponentsEnum.headlineEyebrownText,
          {
            headline1,
            text1,
            eyebrown
          }
        );
    }
    case ComponentsEnum.headlineWithTeaser: {
      return (headline1: string, text1: string) =>
        setFunc<ComponentsEnum.headlineWithTeaser>(
          ComponentsEnum.headlineWithTeaser,
          {
            headline1,
            text1
          }
        );
    }
    case ComponentsEnum.headlineWithTeaser2: {
      return (headline1: string, text1: string) =>
        setFunc<ComponentsEnum.headlineWithTeaser2>(
          ComponentsEnum.headlineWithTeaser2,
          {
            headline1,
            text1
          }
        );
    }
    case ComponentsEnum.h3WithText: {
      return (headline1: string, text1: string) =>
        setFunc<ComponentsEnum.h3WithText>(ComponentsEnum.h3WithText, {
          headline1,
          text1
        });
    }
    case ComponentsEnum.picture: {
      return (
        picture: string,
        text1: string,
        fileStack: FileStackCredentials
      ) =>
        setFunc<ComponentsEnum.picture>(ComponentsEnum.picture, {
          picture,
          text1,
          fileStack
        });
    }
    case ComponentsEnum.picture2: {
      return (
        picture: string,
        picture2: string,
        text1: string,
        text2: string,
        fileStack: FileStackCredentials
      ) =>
        setFunc<ComponentsEnum.picture2>(ComponentsEnum.picture2, {
          picture,
          picture2,
          text1,
          text2,
          fileStack
        });
    }
    case ComponentsEnum.picture3: {
      return (
        picture: string,
        picture2: string,
        picture3: string,
        text1: string,
        text2: string,
        text3: string,
        fileStack: FileStackCredentials
      ) =>
        setFunc<ComponentsEnum.picture3>(ComponentsEnum.picture3, {
          picture,
          picture2,
          picture3,
          text1,
          text2,
          text3,
          fileStack
        });
    }
    case ComponentsEnum.pictureHeadlineText3: {
      return (
        picture: string,
        picture2: string,
        picture3: string,
        headline1: string,
        headline2: string,
        headline3: string,
        text1: string,
        text2: string,
        text3: string,
        fileStack: FileStackCredentials
      ) =>
        setFunc<ComponentsEnum.pictureHeadlineText3>(
          ComponentsEnum.pictureHeadlineText3,
          {
            picture,
            picture2,
            picture3,
            headline1,
            headline2,
            headline3,
            text1,
            text2,
            text3,
            fileStack
          }
        );
    }
    case ComponentsEnum.pictureWithText: {
      return (
        picture: string,
        text1: string,
        text2: string,
        fileStack: FileStackCredentials
      ) =>
        setFunc<ComponentsEnum.pictureWithText>(
          ComponentsEnum.pictureWithText,
          {
            picture,
            text1,
            text2,
            fileStack
          }
        );
    }
    case ComponentsEnum.textBlock: {
      return (text1: string) =>
        setFunc<ComponentsEnum.textBlock>(ComponentsEnum.textBlock, {
          text1
        });
    }
    case ComponentsEnum.textBlock2: {
      return (text1: string, text2: string) =>
        setFunc<ComponentsEnum.textBlock2>(ComponentsEnum.textBlock2, {
          text1,
          text2
        });
    }
  }
}

function getProperties<T extends ComponentsEnum>(
  component: T,
  props: ComponentMap[T]
): ComponentsProps<T>;
function getProperties(
  component: ComponentsEnum,
  props: ComponentMap[ComponentsEnum]
) {
  switch (ComponentsEnum[component]) {
    case ComponentsEnum.headline: {
      const values = props as ComponentMap[ComponentsEnum.headline];
      return setFunc<ComponentsEnum.headline>(ComponentsEnum.headline, values);
    }
    case ComponentsEnum.headlineEyebrownText: {
      const values = props as ComponentMap[ComponentsEnum.headlineEyebrownText];
      return setFunc<ComponentsEnum.headlineEyebrownText>(
        ComponentsEnum.headlineEyebrownText,
        values
      );
    }
    case ComponentsEnum.headlineWithTeaser: {
      const values = props as ComponentMap[ComponentsEnum.headlineWithTeaser];
      return setFunc<ComponentsEnum.headlineWithTeaser>(
        ComponentsEnum.headlineWithTeaser,
        values
      );
    }
    case ComponentsEnum.headlineWithTeaser2: {
      const values = props as ComponentMap[ComponentsEnum.headlineWithTeaser2];
      return setFunc<ComponentsEnum.headlineWithTeaser2>(
        ComponentsEnum.headlineWithTeaser2,
        values
      );
    }
    case ComponentsEnum.h3WithText: {
      const values = props as ComponentMap[ComponentsEnum.h3WithText];
      return setFunc<ComponentsEnum.h3WithText>(
        ComponentsEnum.h3WithText,
        values
      );
    }
    case ComponentsEnum.picture: {
      const values = props as ComponentMap[ComponentsEnum.picture];
      return setFunc<ComponentsEnum.picture>(ComponentsEnum.picture, values);
    }
    case ComponentsEnum.picture2: {
      const values = props as ComponentMap[ComponentsEnum.picture2];
      return setFunc<ComponentsEnum.picture2>(ComponentsEnum.picture2, values);
    }
    case ComponentsEnum.picture3: {
      const values = props as ComponentMap[ComponentsEnum.picture3];
      return setFunc<ComponentsEnum.picture3>(ComponentsEnum.picture3, values);
    }
    case ComponentsEnum.pictureHeadlineText3: {
      const values = props as ComponentMap[ComponentsEnum.pictureHeadlineText3];
      return setFunc<ComponentsEnum.pictureHeadlineText3>(
        ComponentsEnum.pictureHeadlineText3,
        values
      );
    }
    case ComponentsEnum.pictureWithText: {
      const values = props as ComponentMap[ComponentsEnum.pictureWithText];
      return setFunc<ComponentsEnum.pictureWithText>(
        ComponentsEnum.pictureWithText,
        values
      );
    }
    case ComponentsEnum.textBlock: {
      const values = props as ComponentMap[ComponentsEnum.textBlock];
      return setFunc<ComponentsEnum.textBlock>(
        ComponentsEnum.textBlock,
        values
      );
    }
    case ComponentsEnum.textBlock2: {
      const values = props as ComponentMap[ComponentsEnum.textBlock2];
      return setFunc<ComponentsEnum.textBlock2>(
        ComponentsEnum.textBlock2,
        values
      );
    }
  }
}

export const getPropTemplateFunc = getPropTemplate;
export const getPropertiesFunc = getProperties;
