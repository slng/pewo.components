import "bootstrap/dist/css/bootstrap.css";
// tslint:disable-next-line:no-submodule-imports
import "pewo.components/dist/index.css";

import {
  Component,
  ComponentsEnum,
  getProperties,
  getPropTemplate,
  HeadlineEyebrownText,
  HeadlineWithTeaser2,
  H2WithText,
  PictureHeadlineText3
} from "pewo.components";
import React from "react";
import { FileStackCredentials } from "../../src/types";
import {
  FILESTACK_API_KEY,
  FILESTACK_POLICY,
  FILESTACK_SIGNATURE
} from "./filestackCredentials";

// const myTestFunction = (testData: any) => {
//   console.log(testData);
// };

export default () => {
  const fileStack: FileStackCredentials = {
    api: FILESTACK_API_KEY,
    policy: FILESTACK_POLICY,
    signature: FILESTACK_SIGNATURE
  };

  const headLineProps = getPropTemplate<ComponentsEnum.headline>(
    ComponentsEnum.headline
  )("test");

  const headlineWithTeaserProps = getProperties<
    ComponentsEnum.headlineWithTeaser
  >(ComponentsEnum.headlineWithTeaser, {
    headline1: "test2",
    text1: "voll krass"
  });

  const pictureProps = getProperties<ComponentsEnum.picture>(
    ComponentsEnum.picture,
    {
      picture: "mckAPYjdQba9aJskDZDK",
      text1: "voll krass",
      disabled: true,
      fileStack
    }
  );

  const picture2Props = getProperties<ComponentsEnum.picture2>(
    ComponentsEnum.picture2,
    {
      picture: "mckAPYjdQba9aJskDZDK",
      picture2: "mckAPYjdQba9aJskDZDK",
      text1: "voll krass",
      text2: "voll krass Junge",
      disabled: true,
      fileStack
    }
  );

  const pictureWithTextProps = getProperties<ComponentsEnum.pictureWithText>(
    ComponentsEnum.pictureWithText,
    {
      picture: "mckAPYjdQba9aJskDZDK",
      text1: "voll krass",
      text2: "junge",
      fileStack
    }
  );

  const textBlock2PropsFunc = getPropTemplate<ComponentsEnum.textBlock2>(
    ComponentsEnum.textBlock2
  );
  const textBlock2Props = textBlock2PropsFunc("voll krass", "joa man");
  console.log(textBlock2Props);

  // const textBlock2Props = getProperties<ComponentsEnum.text2Block>(
  //   ComponentsEnum.text2Block,
  //   {
  //     text1: "test2",
  //     text2: "voll krass"
  //   }
  // );
  // console.log(textBlock2Props)

  return (
    <div>
      <H2WithText
        headline1={'Hallo'}
        text1={"test texts"}
      />
      <HeadlineEyebrownText
        text1={"Test Text"}
        eyebrown={"eybrown in red?"}
        headline1={"title und so..."}
        className={"mt-5"}
        disabled={true}
      />

      <HeadlineWithTeaser2
        text1={"Test Text"}
        headline1={"title und so..."}
        style={{ marginBottom: 50, textAlign: 'center' }}
      />
      <PictureHeadlineText3
        picture="mckAPYjdQba9aJskDZDK"
        picture2="mckAPYjdQba9aJskDZDK"
        picture3="mckAPYjdQba9aJskDZDK"
        headline1="voll krass"
        headline2="voll krass"
        headline3="voll krass"
        text1="voll krass"
        text3="voll krass"
        text2="voll krass Junge"
        fileStack={fileStack}
      />
      {/*<Component {...headLineProps} />
      <Component {...headlineWithTeaserProps} />
      <Component
        type={ComponentsEnum.headlineWithTeaser2}
        props={{
          headline: "",
          text1: "",
          text2: ""
        }}
        componentObj={{
          modified: false
        }}
        // disabled={false}
      />
      <Component {...pictureProps} />
      <Component {...picture2Props} />
      <Component {...pictureWithTextProps} />
      <Component
        type={ComponentsEnum.textBlock}
        props={{ text1: "sdafsdaf" }}
        componentObj={{ modified: false }}
      />
      <Component {...textBlock2Props} />*/}
    </div>
  );
};
